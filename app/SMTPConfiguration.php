<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMTPConfiguration extends Model
{
	protected $table = 'smtp_configuration';
	protected $guarded = [];
}
