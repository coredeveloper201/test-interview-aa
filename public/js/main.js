new Vue({
	el: '#mail_config',
	data: {
		showSmtpIndex: true,
		addSmtpConfig: false,
		showSmtpConfigBtn: true,
		showSmtpConfigEditBtn: false,
		editSmtpConfig: false,
		showImapIndex: true,
		addImapConfig: false,
		editImapConfig: false,
		currentSmtpId: '',
		smtp_mail_driver: '',
		smtp_mail_host: '',
		smtp_mail_port: '',
		smtp_mail_username: '',
		smtp_mail_password: '',
		smtp_mail_encryption: ''
	},
	methods: {
		addSmtpConfiguration() {
			this.showSmtpIndex = false;
			this.addSmtpConfig = true;
		},
		savedSmtpConfig() {
			var csrf_token = $('meta[name="csrf-token"]').attr('content');
			var dataObj = {'_token': csrf_token, mail_driver: this.smtp_mail_driver, mail_host: this.smtp_mail_host, mail_port: this.smtp_mail_port, mail_username: this.smtp_mail_username, mail_password: this.smtp_mail_password, mail_encryption: this.smtp_mail_encryption };
			var outerThis = this;

			$.ajax({
				type: 'post',
				url: '/smtp',
				data: dataObj,
				async: false,
				success: function(data) {
					if(data === 'success') {
						outerThis.addSmtpConfig = false;
						outerThis.showSmtpIndex = true;
						outerThis.showSmtpConfigBtn = false;
						outerThis.showSmtpConfigEditBtn = true;
					}
				}
			});
		},
		editSmtpConfiguration() {
			this.showSmtpIndex = false;
			this.editSmtpConfig = true;
		},
		updateSmtpConfig() {
			var csrf_token = $('meta[name="csrf-token"]').attr('content');
			var dataObj = {'_token': csrf_token, mail_driver: this.smtp_mail_driver, mail_host: this.smtp_mail_host, mail_port: this.smtp_mail_port, mail_username: this.smtp_mail_username, mail_password: this.smtp_mail_password, mail_encryption: this.smtp_mail_encryption };
			var outerThis = this;

			$.ajax({
				type: 'post',
				url: '/smtp/'+this.currentSmtpId,
				data: dataObj,
				async: false,
				success: function(data) {
					if(data === 'updated') {
						outerThis.editSmtpConfig = false;
						outerThis.showSmtpIndex = true;
					}
      			}
			});
		},
		cancelSmtpConfig() {
			this.showSmtpIndex = true;
			this.editSmtpConfig = false;
			this.addSmtpConfig = false;
		},
		addImapConfiguration() {
			this.showImapIndex = false;
			this.addImapConfig = true;
		},
		editImapConfiguration() {
			this.showImapIndex = false;
			this.editImapConfig = true;
		}
	},
	mounted() {
		console.log(smtpConfig);
		if (smtpConfig !== null) {
			this.currentSmtpId = smtpConfig.id;
			this.smtp_mail_driver = smtpConfig.MAIL_DRIVER;
			this.smtp_mail_host = smtpConfig.MAIL_HOST;
			this.smtp_mail_port = smtpConfig.MAIL_PORT;
			this.smtp_mail_username = smtpConfig.MAIL_USERNAME;
			this.smtp_mail_password = smtpConfig.MAIL_PASSWORD;
			this.smtp_mail_encryption = smtpConfig.MAIL_ENCRYPTION;
			this.showSmtpConfigEditBtn = true;
		}
	}
})