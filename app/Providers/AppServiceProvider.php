<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // if (\Schema::hasTable('smtp_configuration')) {
        //     $smtpDetails = \App\SMTPConfiguration::where('user_id', Auth::user()->id)->first();
        //     \Config::set('smtp_settings.MAIL_DRIVER', $smtpDetails->MAIL_DRIVER);
        //     \Config::set('smtp_settings.MAIL_HOST', $smtpDetails->MAIL_HOST);
        //     \Config::set('smtp_settings.MAIL_PORT', $smtpDetails->MAIL_PORT);
        //     \Config::set('smtp_settings.MAIL_USERNAME', $smtpDetails->MAIL_USERNAME);
        //     \Config::set('smtp_settings.MAIL_PASSWORD', $smtpDetails->MAIL_PASSWORD);
        //     \Config::set('smtp_settings.MAIL_ENCRYPTION', $smtpDetails->MAIL_ENCRYPTION);
        // }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
