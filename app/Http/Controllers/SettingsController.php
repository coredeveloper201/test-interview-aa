<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SMTPConfiguration;

class SettingsController extends Controller
{
	public function index()
	{
		$smtpConfig = SMTPConfiguration::where('user_id', auth()->user()->id)
										 ->orderBy('id', 'desc')
										 ->first();

		if(!empty($smtpConfig)) {
			$smtpConfig = $smtpConfig->toArray();
		}

		return view('settings.index', compact('smtpConfig'));
	}

	public function saveSmtpConfig(Request $request)
	{
		SMTPConfiguration::create([
			'user_id' => auth()->user()->id,
			'MAIL_DRIVER' => $request->mail_driver,
			'MAIL_HOST'=> $request->mail_host,
			'MAIL_PORT'=> $request->mail_port,
			'MAIL_USERNAME'=> $request->mail_username,
			'MAIL_PASSWORD'=> $request->mail_password,
			'MAIL_ENCRYPTION'=> $request->mail_encryption ? $request->mail_encryption : null
		]);

		return "success";
	}

	public function updateSmtpConfig(Request $request, $id=null)
	{
		$editedSmtpData = SMTPConfiguration::find($id);
		$editedSmtpData->MAIL_DRIVER = $request->mail_driver;
		$editedSmtpData->MAIL_HOST = $request->mail_host;
		$editedSmtpData->MAIL_PORT = $request->mail_port;
		$editedSmtpData->MAIL_USERNAME = $request->mail_username;
		$editedSmtpData->MAIL_PASSWORD = $request->mail_password;
		$editedSmtpData->MAIL_ENCRYPTION = $request->mail_encryption ? $request->mail_encryption : null;
		$editedSmtpData->save();

		return "updated";
	}
}
