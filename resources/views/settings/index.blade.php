@extends ('layouts.app')

@section ('title', 'Home Page')

@section ('content')

	<script type="text/javascript">
		var smtpConfig = <?php echo json_encode($smtpConfig); ?>;
	</script>

	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div id="mail_config">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="smtp-tab" data-toggle="tab" href="#smtp_config" role="tab" aria-controls="smtp" aria-selected="true">SMTP Configuration</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="iamp-tab" data-toggle="tab" href="#iamp_config" role="tab" aria-controls="iamp" aria-selected="false">IMAP Configuration</a>
						</li>
					</ul>

					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="smtp_config" role="tabpanel" aria-labelledby="smtp-tab">

							<div class="text-right mt-4">
								@if(empty($smtpConfig))
									<button type="button" class="btn btn-sm btn-primary" @click="addSmtpConfiguration" v-if="showSmtpConfigBtn">Add</button>
								@endif
									<button type="button" class="btn btn-sm btn-info" @click="editSmtpConfiguration" v-if="showSmtpConfigEditBtn">Edit</button>
							</div>


							<div class="table-responsive" v-if="showSmtpIndex">
								<h4 class="mt-2 mb-4">SMTP Details</h4>
								<table class="table table-bordered">
									<thead>
										<colgroup>
											<col width="30%">
											<col width="70%">
										</colgroup>
									</thead>
									<tbody>
										<tr>
											<th>MAIL_DRIVER</th>
											<td class="text-center">@{{ smtp_mail_driver }}</td>
										</tr>
										<tr>
											<th>MAIL_HOST</th>
											<td class="text-center">@{{ smtp_mail_host }}</td>
										</tr>
										<tr>
											<th>MAIL_PORT</th>
											<td class="text-center">@{{ smtp_mail_port }}</td>
										</tr>
										<tr>
											<th>MAIL_USERNAME</th>
											<td class="text-center">@{{ smtp_mail_username }}</td>
										</tr>
										<tr>
											<th>MAIL_PASSWORD</th>
											<td class="text-center">@{{ smtp_mail_password }}</td>
										</tr>
										<tr>
											<th>MAIL_ENCRYPTION</th>
											<td class="text-center">@{{ smtp_mail_encryption }}</td>
										</tr>
									</tbody>
								</table>
							</div>


							<div class="add_smtp_config" v-if="addSmtpConfig">
								<h4 class="mt-2 mb-4">Add Details</h4>
								<div class="form-group">
									<label for="smtp_mail_driver">MAIL DRIVER</label>
									<input type="text" class="form-control" id="smtp_mail_driver" aria-describedby="mailDriverHelp" v-model="smtp_mail_driver">
								</div>

								<div class="form-group">
									<label for="smtp_mail_host">MAIL HOST</label>
									<input type="text" class="form-control" id="smtp_mail_host" aria-describedby="mailHostHelp" v-model="smtp_mail_host">
								</div>

								<div class="form-group">
									<label for="smtp_mail_port">MAIL PORT</label>
									<input type="number" class="form-control" id="smtp_mail_port" aria-describedby="mailPortHelp" v-model="smtp_mail_port">
								</div>

								<div class="form-group">
									<label for="smtp_mail_username">MAIL USERNAME</label>
									<input type="text" class="form-control" id="smtp_mail_username" aria-describedby="mailUserNameHelp" v-model="smtp_mail_username">
								</div>

								<div class="form-group">
									<label for="smtp_mail_password">MAIL PASSWORD</label>
									<input type="password" class="form-control" id="smtp_mail_password" aria-describedby="mailPasswordHelp" v-model="smtp_mail_password">
								</div>

								<div class="form-group">
									<label for="smtp_mail_encryption">MAIL_ENCRYPTION</label>
									<input type="text" class="form-control" id="smtp_mail_encryption" aria-describedby="mailEncryptionHelp" v-model="smtp_mail_encryption">
								</div>

								<div>
									<button type="button" class="btn btn-primary float-left" @click="savedSmtpConfig">Save</button>
									<button type="button" class="btn btn-danger float-right" @click="cancelSmtpConfig">Cancel</button>
								</div>
							</div>


							<div class="edit_smtp_config" v-if="editSmtpConfig">
								<h4 class="mt-2 mb-4">Edit Details</h4>
								<input type="hidden" name="smtp_id" v-model="currentSmtpId">
								<div class="form-group">
									<label for="edit_smtp_mail_driver">MAIL DRIVER</label>
									<input type="text" class="form-control" id="edit_smtp_mail_driver" aria-describedby="mailDriverHelp" v-model="smtp_mail_driver">
								</div>

								<div class="form-group">
									<label for="edit_smtp_mail_host">MAIL HOST</label>
									<input type="text" class="form-control" id="edit_smtp_mail_host" aria-describedby="mailHostHelp" v-model="smtp_mail_host">
								</div>

								<div class="form-group">
									<label for="edit_smtp_mail_port">MAIL PORT</label>
									<input type="text" class="form-control" id="edit_smtp_mail_port" aria-describedby="mailPortHelp" v-model="smtp_mail_port">
								</div>

								<div class="form-group">
									<label for="edit_smtp_mail_username">MAIL USERNAME</label>
									<input type="text" class="form-control" id="edit_smtp_mail_username" aria-describedby="mailUserNameHelp" v-model="smtp_mail_username">
								</div>

								<div class="form-group">
									<label for="edit_smtp_mail_password">MAIL PASSWORD</label>
									<input type="text" class="form-control" id="edit_smtp_mail_password" aria-describedby="mailPasswordHelp" v-model="smtp_mail_password">
								</div>

								<div class="form-group">
									<label for="edit_smtp_mail_encryption">MAIL_ENCRYPTION</label>
									<input type="text" class="form-control" id="edit_smtp_mail_encryption" aria-describedby="mailEncryptionHelp" v-model="smtp_mail_encryption">
								</div>

								<div>
									<button type="button" class="btn btn-primary float-left" @click="updateSmtpConfig">Update</button>
									<button type="button" class="btn btn-danger float-right" @click="cancelSmtpConfig">Cancel</button>
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="iamp_config" role="tabpanel" aria-labelledby="iamp-tab">

							<div class="text-right mt-4">
								<button type="button" class="btn btn-sm btn-primary" @click="addImapConfiguration">Add</button>
								<button type="button" class="btn btn-sm btn-info" @click="editImapConfiguration">Edit</button>
							</div>


							<div class="table-responsive" v-if="showImapIndex">
								<h4>IMAP Details</h4>
								<table class="table table-bordered">
									<thead>
										<colgroup>
											<col width="30%">
											<col width="70%">
										</colgroup>
									</thead>
									<tbody>
										<tr>
											<th>IMAP_HOST</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_ENCRYPTION</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_PORT</th>
											<td>2525</td>
										</tr>
										<tr>
											<th>IMAP_VALIDATE_CERT</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_USERNAME</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_PASSWORD</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_DEFAULT_ACCOUNT</th>
											<td>IMAP</td>
										</tr>
										<tr>
											<th>IMAP_PROTOCOL</th>
											<td>IMAP</td>
										</tr>
									</tbody>
								</table>
							</div>


							<div class="add_imap_config" v-if="addImapConfig">
								<h4 class="mt-4 mb-4">Add IMAP Details</h4>
								<form>
									<div class="form-group">
										<label for="imap_host">IMAP HOST</label>
										<input type="text" class="form-control" id="imap_host" aria-describedby="imapHostHelp">
									</div>

									<div class="form-group">
										<label for="imap_encryption">IMAP ENCRYPTION</label>
										<input type="text" class="form-control" id="imap_encryption" aria-describedby="imapEncryptionHelp">
									</div>

									<div class="form-group">
										<label for="imap_port">IMAP PORT</label>
										<input type="text" class="form-control" id="imap_port" aria-describedby="imapPortHelp">
									</div>

									<div class="form-group">
										<label for="imap_validate_cert">IMAP VALIDATE CERT</label>
										<input type="text" class="form-control" id="imap_validate_cert" aria-describedby="imapValidateCertHelp">
									</div>

									<div class="form-group">
										<label for="imap_username">IMAP USERNAME</label>
										<input type="text" class="form-control" id="imap_username" aria-describedby="imapUsernamedHelp">
									</div>

									<div class="form-group">
										<label for="imap_password">MAIL PASSWORD</label>
										<input type="text" class="form-control" id="imap_password" aria-describedby="imapPasswordHelp">
									</div>

									<div class="form-group">
										<label for="imap_default_account">IMAP_DEFAULT_ACCOUNT</label>
										<input type="text" class="form-control" id="imap_default_account" aria-describedby="imapDefaultAccountnHelp">
									</div>

									<div class="form-group">
										<label for="imap_protocol">IMAP_PROTOCOL</label>
										<input type="text" class="form-control" id="imap_protocol" aria-describedby="imapProtocolHelp">
									</div>

									<button type="submit" class="btn btn-primary">Submit</button>
								</form>
							</div>


							<div class="edit_imap_config" v-if="editImapConfig">
								<h4 class="mt-4 mb-4">Edit IMAP Details</h4>
								<form>
									<div class="form-group">
										<label for="edit_imap_host">IMAP HOST</label>
										<input type="text" class="form-control" id="edit_imap_host" aria-describedby="imapHostHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_encryption">IMAP ENCRYPTION</label>
										<input type="text" class="form-control" id="edit_imap_encryption" aria-describedby="imapEncryptionHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_port">IMAP PORT</label>
										<input type="text" class="form-control" id="edit_imap_port" aria-describedby="imapPortHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_validate_cert">IMAP VALIDATE CERT</label>
										<input type="text" class="form-control" id="edit_imap_validate_cert" aria-describedby="imapValidateCertHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_username">IMAP USERNAME</label>
										<input type="text" class="form-control" id="edit_imap_username" aria-describedby="imapUsernamedHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_password">MAIL PASSWORD</label>
										<input type="text" class="form-control" id="edit_imap_password" aria-describedby="imapPasswordHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_default_account">IMAP DEFAULT ACCOUNT</label>
										<input type="text" class="form-control" id="edit_imap_default_account" aria-describedby="imapDefaultAccountnHelp">
									</div>

									<div class="form-group">
										<label for="edit_imap_protocol">IMAP PROTOCOL</label>
										<input type="text" class="form-control" id="edit_imap_protocol" aria-describedby="imapProtocolHelp">
									</div>

									<button type="submit" class="btn btn-primary">Update</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection