<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImapConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imap_configuration', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('IMAP_HOST');
            $table->integer('IMAP_PORT');
            $table->string('IMAP_ENCRYPTION');
            $table->boolean('IMAP_VALIDATE_CERT')->default(true);
            $table->string('IMAP_USERNAME');
            $table->string('IMAP_PASSWORD');
            $table->string('IMAP_DEFAULT_ACCOUNT')->default('default');
            $table->string('IMAP_PROTOCOL')->default('imap');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imap_configuration');
    }
}
