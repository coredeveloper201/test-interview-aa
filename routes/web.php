<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function() {

	Route::get('/', 'HomeController@index')->name('landing_page');
	Route::get('/settings', 'SettingsController@index')->name('settings');
	Route::post('/smtp', 'SettingsController@saveSmtpConfig');
	Route::post('/smtp/{smtpId}', 'SettingsController@updateSmtpConfig');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
