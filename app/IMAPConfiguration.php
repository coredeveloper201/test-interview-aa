<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IMAPConfiguration extends Model
{
    protected $table = 'imap_configuration';
    protected $guarded = [];
}
